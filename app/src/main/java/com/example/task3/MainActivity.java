package com.example.task3;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import org.w3c.dom.Text;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    TextView show;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Button a_as_listen = (Button) findViewById(R.id.ACTICITY_as_listen);
        Button neibu_as_listen = findViewById(R.id.neibulei_as_listen);
        Button niming_as_listen= findViewById(R.id.nimingn_as_listen);
        Button lambda_as_listen= findViewById(R.id.LAMBDA_as_listen);
        Button waibu_as_listen= findViewById(R.id.waibulei_as_listen);
        a_as_listen.setOnClickListener(this);
        Button Systeminfo=findViewById(R.id.xianshixitong);
        Button Progressbar=findViewById(R.id.xianshijindutiao);
        Progressbar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent startProgress=new Intent(MainActivity.this,ProgressBar.class);
                startActivity(startProgress);
            }
        });
        Systeminfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent startSysteminfo=new Intent(MainActivity.this,SystemInfo.class);
                startActivity(startSysteminfo);
            }
        });
        show = (TextView) findViewById(R.id.text_2);
        neibu_as_listen.setOnClickListener(new MyOnClickListener());
        niming_as_listen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                show.setText("匿名内部类作为监听器");
            }
        });
        lambda_as_listen.setOnClickListener(view -> show.setText("LAMBDA表达式作为监听器"));
        waibu_as_listen.setOnClickListener(new OuterClassListener(show));

    }

    @Override
    public void onClick(View v) {
        show.setText("ACTIVITY作为监听器");
    }

    private class MyOnClickListener implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.neibulei_as_listen:
                    show.setText("内部类作为监听器");
                default:
                    break;
            }

        }
    }
    public void clickHandler(View v){

        show.setText("监听器绑定到标签");
    }
}
class OuterClassListener implements  View.OnClickListener{
    TextView show;
    public OuterClassListener(TextView show){
        this.show=show;
    }
    @Override
    public void onClick(View v) {
       show.setText("外部类作为监听器");
    }
}



